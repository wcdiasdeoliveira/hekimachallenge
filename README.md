# HekimaChallenge

## Synopsis

Este repositório explica de maneira simplificada um esboço arquitetural de fácil evolução para atender os requisitos dos 
exercícios propostos. 

Dado o prazo disponível, preferi explorar em maior profundidade a disposição( desenho arquitetural ) dos elementos do que sua implementação, 
que considero razoavelmente simples.  

Uma aplicação nestes moldes pode ser facilmente expandida e construída de maneiras diversas.

### EX1

Ambas soluções( ex1 e ex2 ) consideram primariamente 2 componentes: um ApiGateway e um backEnd isolado.

O ApiGateway( baseado em HATEOAS ) cuida do roteamento e exposição dos serviços bem como sua tolerância à falhas. Dispondo os serviços através 
de sua interface, é possível, por exemplo, ter um controle mais efetivo de acesso aos serviços, sem mencionar a 
facilidade em rotear versões do serviço.

Ex: urlToService?version=v1 ou urlToService?version=v2

Para corresponder ao desafio do EX1, o backEnd deve operar sobre um cluster de dados, o que aumentaria a disponibilidade e 
resiliência do serviço.

Para entregar eficiência, seu design se apoia em disponibilidade, baixo acoplamento e estrutura que dê suporte à expansão, tanto 
em carga como para suportar novos componentes.

Vide diagramas de componente e implantação.

### EX2

A partir da solução anterior, adicionamos um loadBalancer antes do ApiGateway e multiplicamos suas instâncias. 
Degradações na perfomance ou latência dos serviços poderiam ser observadas e as requisições roteadas para instâncias mais eficientes.

No mesmo sentido, instâncias do backEnd poderiam ser acrescidas horizontalmente.

Acredito que essas modificações endereçam o desafio do EX2.

### Trade offs

As escolhas da arquitetura almejam ter um ambiente distribuído, principalmente. Como os componentes são isolados, seu deploy e 
manutenção podem ser feitos nó a nó, serviço a serviço.

Este arcabouço permite inclusive a adoção de testes como os da Simian Army, da Netflix, para testar falhas de cada componente do 
desenho. A solução pode ser implementada em qualquer linguagem e idealmente, seria poliglota: componentes em várias linguagens, usando 
o melhor de cada plataforma.

Evidentemente, com o aumento do número de componentes cresce também a complexidade em mantê-los e escalá-los.

Cada aplicação pode ser exposta através de conteinneres Docker.

## Installation

Clone o repositório:

git clone https://bitbucket.org/wcdiasdeoliveira/hekimachallenge

Dentro do respectivo diretório, execute:

./gradlew build && java -jar build/libs/<APP_TO_RUN>.jar OR ./gradlew build run

## Contributors

Wesley C. Dias de Oliveira

## License

Free for use and extend.