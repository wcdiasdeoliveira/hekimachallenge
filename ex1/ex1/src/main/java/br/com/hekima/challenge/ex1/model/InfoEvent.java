package br.com.hekima.challenge.ex1.model;

import java.io.Serializable;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class InfoEvent extends ResourceSupport implements Serializable {
	
	private final static long serialVersionUID = 1l;
	private Long pageId;
	private Long clickId;
	private String eventTimestamp;
	private String expandedData;
	private String message;
	
    @JsonCreator
	public InfoEvent(@JsonProperty("pageId")Long pageId, @JsonProperty("clickId")Long clickId, 
			@JsonProperty("eventTimestamp")String eventTimestamp,
			@JsonProperty("expandedData")String expandedData ) {
		this.setPageId(pageId);
		this.setClickId(clickId);
		this.setEventTimestamp(eventTimestamp);
		this.setExpandedData(expandedData);
		this.setMessage("");
	}

	public Long getPageId() {
		return pageId;
	}
	
	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}
	
	public Long getClickId() {
		return clickId;
	}
	
	public void setClickId(Long clickId) {
		this.clickId = clickId;
	}
	
	public String getEventTimestamp() {
		return eventTimestamp;
	}
	
	public void setEventTimestamp(String eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}
	
	public String getExpandedData() {
		return expandedData;
	}
	
	public void setExpandedData(String jsonExpandedData) {
		this.expandedData = jsonExpandedData;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
    public String toString() {
		 try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	 }
}