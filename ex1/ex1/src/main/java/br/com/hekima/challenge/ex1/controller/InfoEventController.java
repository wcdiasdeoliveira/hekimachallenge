package br.com.hekima.challenge.ex1.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import br.com.hekima.challenge.ex1.model.InfoEvent;

@RestController
public class InfoEventController {

	public HttpEntity<InfoEvent> fallBackSaveInfoEvent(Long pageId, Long clickId, String eventTimestamp, String expandedData){		
    	InfoEvent fallBackevent = new InfoEvent(pageId, clickId, eventTimestamp, expandedData);
    	fallBackevent.setMessage("'event' out of service.");
		return new ResponseEntity<>(fallBackevent, HttpStatus.OK);
	}
	
	@HystrixCommand(fallbackMethod = "fallBackSaveInfoEvent")
    @RequestMapping("/event")
	public HttpEntity<InfoEvent> saveInfoEvent(@RequestParam(value = "pageId", required = true) Long pageId, 
			@RequestParam(value = "clickId", required = true) Long clickId,
			@RequestParam(value = "eventTimestamp", required = true) String eventTimestamp,
			@RequestParam(value = "expandedData", required = false) String expandedData){   
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<InfoEvent> response = restTemplate.exchange(
	    		"backend-service", HttpMethod.GET, null, new ParameterizedTypeReference<InfoEvent>() {}
	    );		
    	InfoEvent event = new InfoEvent(pageId, clickId, eventTimestamp, expandedData);
		event.add(linkTo(methodOn(InfoEventController.class).saveInfoEvent(pageId, clickId, eventTimestamp, expandedData)).withSelfRel());
        return new ResponseEntity<>(response.getBody(), HttpStatus.OK);
	}
}
